package au.com.ephox;

import au.com.ephox.map.EMap;
import au.com.ephox.map.EMapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;

/**
 * EMap Main Entry
 */
public class EMapUI {

    private static final Logger LOG = LoggerFactory.getLogger(EMapUI.class);
    private static final String initMapJsonString = "{\n" + "\"map\":\n" + "[\n" + "{\"A\":\n" + "{\n" + "\"B\":\n" + "100,\n" + "\"C\":\n" + "30\n" + "}},\n" + "{\"B\":\n" + "{\n" + "\"F\":\n" + "300}},\n" + "{\"C\":\n" + "{\n" + "\"D\":\n" + "200}},\n" + "{\"D\":\n" + "{\n" + "\"H\":\n" + "90,\n" + "\"E\":80}},\n" + "{\"E\":\n" + "{\n" + "\"H\":\n" + "30,\n" + "\"G\":150,\n" + "\"F\":50}},\n" + "{\"F\":\n" + "{\n" + "\"G\":70}},\n" + "{\"G\":\n" + "{\n" + "\"H\":50}}\n" + "]\n" + "}";
    
    public static void main(String[] args) {
        //initialize map 
        EMap eMap = new EMap(initMapJsonString);

        //Welcome to Ephox Map system
        System.out.println("================================================");
        System.out.println("Welcome to Ephox Map System !");
        System.out.println("================================================");

        // Choose your operation
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Please choose your operation : ");
            System.out.println("");
            System.out.println("(A) Calculate the shortest distance between any cities.");
            System.out.println("(B) Add a new route.");
            System.out.println("(C) Update a existing route.");
            System.out.println("(D) Delete a existing route.");
            System.out.println("");
            String choice = scanner.next();
            System.out.println("Please enter json input : ");
            String inputParams = scanner.next();
            System.out.println("-----------------------------------------------");
            if ("A".equalsIgnoreCase(choice))
                EMapUtils.showTheShortestRouteFromJson(inputParams, eMap);
            if ("B".equalsIgnoreCase(choice))
                EMapUtils.addRouteFromJson(inputParams, eMap);
            if ("C".equalsIgnoreCase(choice))
                EMapUtils.updateRouteFromJson(inputParams, eMap);
            if ("D".equalsIgnoreCase(choice))
                EMapUtils.deleteRouteFromJson(inputParams, eMap);
        }
    }
}


