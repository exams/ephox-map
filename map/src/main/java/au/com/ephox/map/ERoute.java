package au.com.ephox.map;

/**
 * ERoute class used to store a route information, such as from city, to city and distance between the cities.
 * * 
 */
public class ERoute {

    private String fromCity;
    
    private String toCity;
    
    private int distance;

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
}
