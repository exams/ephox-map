package au.com.ephox.map;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * EMap utilities that help developers easily read input json string and perform CRUD operations
 */
public class EMapUtils {

    private static final Logger LOG = LoggerFactory.getLogger(EMapUtils.class);
    private static final String INVALID_JSON_EXCEPTION_MSG = "Please ensure your json string is valid";

    /**
     * read json string and and use the json data as input parameters to calculate the shortest route between two cities.
     *
     * @param jsonInputString input parameters used to calculate the shortest distance between "from" city to "to" city
     * @param eMap instance of EMap that provide CRUD operations on the map
     */
    public static void showTheShortestRouteFromJson(String jsonInputString, EMap eMap) {
        LOG.debug("Calculate the shortest route based on json : " + jsonInputString);
        if (!isValidJsonString(jsonInputString)) 
            throw new RuntimeException(INVALID_JSON_EXCEPTION_MSG);
        
        Map<String, Object> realTimeMap = new Gson().fromJson(jsonInputString, new TypeToken<HashMap<String, Object>>() {}.getType());
        Collection entrySet = realTimeMap.entrySet();
        Iterator<Map.Entry<String, Object>> it = entrySet.iterator();
        String start = "";
        String end = "";
        while (it.hasNext()) {
            Map.Entry<String, Object> entry = it.next();
            if ("start".equals(entry.getKey())) {
                start = entry.getValue().toString();
            } else {
                end = entry.getValue().toString();
            }
        }
        HashMap theShortestRouteMap = (HashMap<String, Integer>)eMap.showTheShortetRoute(start, end);
        entrySet = theShortestRouteMap.entrySet();
        it = entrySet.iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> entry = it.next();
            String routes = entry.getKey();
            Integer shortestDistance = (Integer)entry.getValue();
            System.out.println("\nThe shortest distance : \n" + routes + "\tdistance : "
                    + shortestDistance + "KM");
            System.out.println(" ----- ----- ------ ------ -----");
        }
    }

    /**
     * read json string and and use the json data as input parameters to update distance between any cities.
     *
     * @param jsonInputString input parameters used to update distance between any cities
     * @param eMap instance of EMap that provide CRUD operations on the map
     */
    public static void updateRouteFromJson(String jsonInputString, EMap eMap) {
        LOG.debug("Update a existing route based on json : " + jsonInputString);
        if (!isValidJsonString(jsonInputString))
            throw new RuntimeException(INVALID_JSON_EXCEPTION_MSG);
        
        Map<String, Object> updateMap = new Gson().fromJson(jsonInputString, new TypeToken<HashMap<String, Object>>() {}.getType());
        Collection entrySet = updateMap.entrySet();
        Iterator<Map.Entry<String, Object>> it = entrySet.iterator();
        Map.Entry<String, Object> entry;
        while (it.hasNext()) {
            entry = it.next();
            String startPoint = entry.getKey();
            LinkedTreeMap paramMap = (LinkedTreeMap) entry.getValue();
            entrySet = paramMap.entrySet();
            it = entrySet.iterator();
            while (it.hasNext()) {
                entry = it.next();
                String endPoint = entry.getKey();
                Double d = (Double) entry.getValue();
                int distance = d.intValue();
                eMap.updateRoute(startPoint, endPoint, distance);
            }
        }
    }

    /**
     * read json string and and use the json data as input parameters to add a new route on the map.
     *
     * @param jsonInputString input parameters used to add a new route on the map
     * @param eMap instance of EMap that provide CRUD operations on the map
     */
    public static void addRouteFromJson(String jsonInputString, EMap eMap) {
        LOG.debug("Add a new route based on json : " + jsonInputString);
        if (!isValidJsonString(jsonInputString))
            throw new RuntimeException(INVALID_JSON_EXCEPTION_MSG);
        
        Map<String, Object> initMap = new Gson().fromJson(jsonInputString, new TypeToken<HashMap<String, Object>>() {}.getType());
        for (Map.Entry<String, Object> entry : initMap.entrySet()) {
            List<LinkedTreeMap> paramList = (ArrayList<LinkedTreeMap>) entry.getValue();
            for (Object o : paramList) {
                LinkedTreeMap paramMap = (LinkedTreeMap) o;
                Collection entrySet = paramMap.entrySet();
                Iterator<Map.Entry<String, Object>> it = entrySet.iterator();
                while (it.hasNext()) {
                    entry = it.next();
                    String startPoint = entry.getKey();
                    paramMap = (LinkedTreeMap) entry.getValue();
                    entrySet = paramMap.entrySet();
                    it = entrySet.iterator();
                    while (it.hasNext()) {
                        entry = it.next();
                        String endPoint = entry.getKey();
                        Double d = (Double) entry.getValue();
                        int distance = d.intValue();
                        eMap.addRoute(startPoint, endPoint, distance);
                    }
                }
            }
        }
    }

    /**
     * read json string and and use the json data as input parameters to delete a existing route on the map.
     *
     * @param jsonInputString input parameters used to delete a existing route on the map
     * @param eMap instance of EMap that provide CRUD operations on the map
     */
    public static void deleteRouteFromJson(String jsonInputString, EMap eMap) {
        LOG.debug("Delete an existing route based on json : " + jsonInputString);
        if (!isValidJsonString(jsonInputString))
            throw new RuntimeException(INVALID_JSON_EXCEPTION_MSG);
        
        Map<String, Object> paramMap = new Gson().fromJson(jsonInputString, new TypeToken<HashMap<String, Object>>() {}.getType());
        Collection entrySet = paramMap.entrySet();
        Iterator<Map.Entry<String, Object>> it = entrySet.iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> entry = it.next();
            String startPoint = entry.getKey();
            String endPoint = entry.getValue().toString();
            eMap.deleteRoute(startPoint, endPoint);
        }
    }

    /**
     * Validate a json string.
     *
     * @param jsonString json string need to be validated
     * @return return true is the given json string is valid
     */
    public static boolean isValidJsonString(String jsonString) {
        try {
            new JsonParser().parse(jsonString);
            return true;
        } catch (JsonSyntaxException jse) {
            return false;
        }
    }
}
