package au.com.ephox.map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;

/**
 * Test cases to test EMap class
 */
public class EMapTest {

    private static final String initMapJsonString = "{\n" + "\"map\":\n" + "[\n" + "{\"A\":\n" + "{\n" + "\"B\":\n" + "100,\n" + "\"C\":\n" + "30\n" + "}},\n" + "{\"B\":\n" + "{\n" + "\"F\":\n" + "300}},\n" + "{\"C\":\n" + "{\n" + "\"D\":\n" + "200}},\n" + "{\"D\":\n" + "{\n" + "\"H\":\n" + "90,\n" + "\"E\":80}},\n" + "{\"E\":\n" + "{\n" + "\"H\":\n" + "30,\n" + "\"G\":150,\n" + "\"F\":50}},\n" + "{\"F\":\n" + "{\n" + "\"G\":70}},\n" + "{\"G\":\n" + "{\n" + "\"H\":50}}\n" + "]\n" + "}";
    EMap eMap;

    @Before
    public void setUp() throws Exception {
    }

    /**
     * Test Case 1 (Calculation)
     * 
     * Test if the api can determine the shortest distance between any cities
     *
     * The given input json ==>  {"start":"A","end":"F"}
     */
    @Test
    public void testCalculation() {
        eMap = new EMap(initMapJsonString);
        String inputParams = "{\"start\":\"A\",\"end\":\"F\"}";
        EMapUtils.showTheShortestRouteFromJson(inputParams, eMap);
        HashMap<String, Integer> theShortestRoutesAndDistance = (HashMap<String, Integer>)eMap.showTheShortetRoute("A", "F");
        Collection entrySet = theShortestRoutesAndDistance.entrySet();
        Iterator<Map.Entry<String, Object>> it = entrySet.iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> entry = it.next();
            String routes = entry.getKey();
            Integer shortestDistance = (Integer)entry.getValue();
            assertEquals(360, shortestDistance.intValue());
            assertEquals("A->C->D->E->F", routes);
        }
    }

    /**
     * Test Case 2 (Update a route)
     *
     * Test if the api can calculate results when map data is changed
     *
     * The given input json ==>  {"A":{"B":10}}
     */
    @Test
    public void testCalculateResultsAfterMapDataUpdated() {
        eMap = new EMap(initMapJsonString);
        String inputParams = "{\"A\":{\"B\":10}}";
        EMapUtils.updateRouteFromJson(inputParams, eMap);
        HashMap<String, Integer> theShortestRoutesAndDistance = (HashMap<String, Integer>)eMap.showTheShortetRoute("A", "F");
        Collection entrySet = theShortestRoutesAndDistance.entrySet();
        Iterator<Map.Entry<String, Object>> it = entrySet.iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> entry = it.next();
            String routes = entry.getKey();
            Integer shortestDistance = (Integer)entry.getValue();
            assertEquals(310, shortestDistance.intValue());
            assertEquals("A->B->F", routes);
        }
    }

    /**
     * Test Case 3 (Add a new route)
     *
     * Test if the api can add new destinations
     *
     * The given input json ==>  {"map":[{"A":{"I":70,"J":150}}]}
     */
    @Test
    public void testCalculateResultsAfterAddingNewDestinations() {
        eMap = new EMap(initMapJsonString);
        String inputParams = "{\"map\":[{\"A\":{\"I\":70,\"J\":150}}]}";
        EMapUtils.addRouteFromJson(inputParams, eMap);
        HashMap<String, Integer> theShortestRoutesAndDistance = (HashMap<String, Integer>)eMap.showTheShortetRoute("A", "I");
        Collection entrySet = theShortestRoutesAndDistance.entrySet();
        Iterator<Map.Entry<String, Object>> it = entrySet.iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> entry = it.next();
            String routes = entry.getKey();
            Integer shortestDistance = (Integer)entry.getValue();
            assertEquals(70, shortestDistance.intValue());
            assertEquals("A->I", routes);
        }
    }

    /**
     * Test Case 4 (Delete a route)
     *
     * Test if the api can delete routes
     *
     * The given input json ==>  {"A":"C","C":"D"}
     */
    @Test
    public void testCalculateResultsAfterDeletingRoute() {
        eMap = new EMap(initMapJsonString);
        String inputParams = "{\"A\":\"C\",\"C\":\"D\"}";
        EMapUtils.deleteRouteFromJson(inputParams, eMap);
        HashMap<String, Integer> theShortestRoutesAndDistance = (HashMap<String, Integer>)eMap.showTheShortetRoute("A", "F");
        Collection entrySet = theShortestRoutesAndDistance.entrySet();
        Iterator<Map.Entry<String, Object>> it = entrySet.iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> entry = it.next();
            String routes = entry.getKey();
            Integer shortestDistance = (Integer)entry.getValue();
            assertEquals(400, shortestDistance.intValue());
            assertEquals("A->B->F", routes);
        }
    }

    /**
     * Test Case 5 (Throw exception if input json string is invalid)
     *
     * Test if the api can delete routes
     *
     * The given input json ==> {"start":"A"
     */
    @Test(expected = RuntimeException.class)
    public void testIfExceptionThrownWhenInputJsonIsInvalid() {
        eMap = new EMap(initMapJsonString);
        String inputParams = "{\"start\":\"A\"";
        EMapUtils.showTheShortestRouteFromJson(inputParams, eMap);
    }

    @After
    public void tearDown() throws Exception {
    }
}
