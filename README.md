# EMap System

A Java console based application that can determine the shortest distance between any places on the map.

## Contributors

* [Lu Li](mailto:hobartlul@gmail.com)

## Dependencies

* Internet Access
* Install Apache Maven [Getting Started and General Configuration](https://maven.apache.org/install.html) for more information.

## Getting Started

1\. In your local machine, navigate to the `/ephox-map` directory. Commands to build the application are performed here.

2\. Use the following commands to build and run the application.

| Command | |
|---|---|
| `mvn clean all` | This is the main command to run the build and junit test tasks. |
| `cd map/` | Navigate to sub module "map" |
| `mvn exec:java` | Run the application. |

## Testing

Please see test cases in [au.com.ephox.map.EMapTest](https://gitlab.com/exams/ephox-map/blob/master/map/src/test/java/au/com/ephox/map/EMapTest.java)

## Improvement

Need to improve input json parameters validation and parsing


